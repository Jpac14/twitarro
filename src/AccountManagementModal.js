import React, {useState, useRef} from "react"

import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalCloseButton,
  ModalBody,
  ModalFooter,
  FormControl,
  FormLabel,
  FormHelperText,
  Divider,
  Center,
  Input,
  InputGroup,
  InputLeftElement,
  InputRightElement,
  Tabs,
  TabList,
  Tab,
  TabPanels,
  TabPanel,
  Flex,
  Button,
  useToast,
} from "@chakra-ui/react"

import {FcGoogle} from "react-icons/fc"
import {MdEmail, MdLock} from "react-icons/md"

import {auth, db} from "./firebase"
import {useAuthState} from "react-firebase-hooks/auth"
import {
  signInWithPopup,
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  GoogleAuthProvider,
} from "firebase/auth"
import {doc, setDoc} from "firebase/firestore"

import {isEmail, isStrongPassword} from "validator"

const faker = require("faker")

export function AccountManagementModal({isOpen, onClose, defaultTab}) {
  const createAccountInitialRef = useRef()
  const loginInitialRef = useRef()

  useAuthState(auth)

  const common = {
    onClose: onClose,
    parentEmail: useState(""),
    parentPassword: useState(""),
    parentLoading: useState(false),
    toast: useToast(),
  }

  return (
    <Modal
      isCentered
      isOpen={isOpen}
      onClose={onClose}
      initialFocusRef={defaultTab == 0 ? createAccountInitialRef : loginInitialRef}
    >
      <ModalOverlay />
      <ModalContent>
        <Tabs isFitted variant="line" defaultIndex={defaultTab}>
          <Flex>
            <TabList width={"90%"}>
              <Tab>Create Account</Tab>
              <Tab>Login</Tab>
            </TabList>
            <Center pl={1}>
              <ModalCloseButton />
            </Center>
          </Flex>
          <TabPanels>
            <TabPanel>
              <Template isCreateAccount initialRef={createAccountInitialRef} {...common} />
            </TabPanel>
            <TabPanel>
              <Template initialRef={loginInitialRef} {...common} />
            </TabPanel>
          </TabPanels>
        </Tabs>
      </ModalContent>
    </Modal>
  )
}

function Template({
  initialRef,
  onClose,
  isCreateAccount,
  parentEmail,
  parentPassword,
  parentLoading,
  toast,
}) {
  const [email, setEmail] = parentEmail
  const [emailValid, setEmailValid] = useState(true)

  const [password, setPassword] = parentPassword
  const [passwordValid, setPasswordValid] = useState(true)

  const [loading, setLoading] = parentLoading
  const [show, setShow] = useState(false)

  const toastWrapper = (title, status, description = "") => {
    toast({
      title: title,
      description: description,
      status: status,
      position: "bottom-left",
    })
  }

  const authHandler = async (result) => {
    toastWrapper(isCreateAccount ? "Account Created" : "Login Sucessful", "success")
    setLoading(false)
    onClose()

    if (!isCreateAccount) return
    const returnNonNull = (a, b) => (a == null ? b : a)

    const username = returnNonNull(auth.currentUser.displayName, faker.internet.userName())
    const profileURL = returnNonNull(
      auth.currentUser.profileURL,
      `https://avatars.dicebear.com/api/adventurer/${username}.svg`
    )

    await setDoc(doc(db, "users", auth.currentUser.uid), {
      username: username,
      profileURL: profileURL,
    })
  }

  const errorHandler = (error) => {
    setLoading(false)
    toastWrapper(`Error: ${error.code}`, "error", error.message)
  }

  const googleAuth = () => {
    setLoading(true)
    const provider = new GoogleAuthProvider()
    signInWithPopup(auth, provider).then(authHandler).catch(errorHandler)
  }

  const emailAuth = (e) => {
    e.preventDefault()

    let errored = false

    if (!isEmail(email)) {
      errored = true
      setEmailValid(false)
      toastWrapper("Invalid Email", "error")
    }
    if (isCreateAccount && !isStrongPassword(password)) {
      errored = true
      setPasswordValid(false)
      toastWrapper("Invalid Password", "error")
    }
    if (errored) return

    setLoading(true)

    const wrapper = (func) => func(auth, email, password).then(authHandler).catch(errorHandler)
    isCreateAccount ? wrapper(createUserWithEmailAndPassword) : wrapper(signInWithEmailAndPassword)
  }

  return (
    <>
      <ModalBody pb={6}>
        <Center pb={4}>
          <Button isLoading={loading} onClick={googleAuth} leftIcon={<FcGoogle />} variant="solid">
            {isCreateAccount ? "Create Account" : "Login"} with Google
          </Button>
        </Center>
        <Divider mb={4} />
        <FormControl onSubmit={emailAuth}>
          <FormLabel>Email</FormLabel>
          <InputGroup>
            <InputLeftElement pointerEvents={"none"} children={<MdEmail />} />
            <Input
              value={email}
              type={"email"}
              ref={initialRef}
              onChange={(e) => {
                setEmail(e.target.value)
                setEmailValid(true)
              }}
              isInvalid={!emailValid}
              placeholder="Email"
            />
          </InputGroup>
          {isCreateAccount && <FormHelperText>We'll never share your email.</FormHelperText>}
        </FormControl>
        <FormControl mt={4}>
          <FormLabel>Password</FormLabel>
          <InputGroup>
            <InputLeftElement pointerEvents={"none"} children={<MdLock />} />
            <Input
              value={password}
              onChange={(e) => {
                setPassword(e.target.value)
                setPasswordValid(true)
              }}
              isInvalid={!passwordValid}
              pr={"4.5rem"}
              type={show ? "text" : "password"}
              placeholder="Password"
            />
            <InputRightElement width={"4.5rem"} onClick={() => setShow(!show)}>
              <Button height={"1.75rem"} size={"sm"}>
                {show ? "Hide" : "Show"}
              </Button>
            </InputRightElement>
          </InputGroup>
          {isCreateAccount && (
            <FormHelperText>
              Must contain one of each: uppercase letter, lowercase letter, symbol, number. Min
              length 8 characters.
            </FormHelperText>
          )}
        </FormControl>
      </ModalBody>
      <ModalFooter pt={0}>
        <Button isLoading={loading} onClick={emailAuth} colorScheme="blue">
          {isCreateAccount ? "Create Account" : "Login"}
        </Button>
      </ModalFooter>
    </>
  )
}
