import React, {useState} from "react"

import {ChakraProvider, Button, IconButton, useDisclosure} from "@chakra-ui/react"
import {theme} from "./theme"

import {ColorModeSwitcher} from "./ColorModeSwitcher"
import {AccountManagementModal} from "./AccountManagementModal"
import {AccountSettingModal} from "./AccountSettingModal"
import {LogoutButton} from "./LogoutButton"

import {MdSettings} from "react-icons/md"

import {auth} from "./firebase"

function App() {
  const accountManagementModal = useDisclosure()
  const [index, setIndex] = useState(0)
  const openModal = (index) => {
    setIndex(index)
    accountManagementModal.onOpen()
  }

  const accountSettingModal = useDisclosure()

  return (
    <ChakraProvider theme={theme}>
      <ColorModeSwitcher />
      {auth.currentUser ? (
        <>
          <LogoutButton />
          <IconButton onClick={accountSettingModal.onOpen} icon={<MdSettings />} />
        </>
      ) : (
        <>
          <Button onClick={() => openModal(0)}>Create Account</Button>
          <Button onClick={() => openModal(1)}>Login</Button>
        </>
      )}
      <AccountManagementModal defaultTab={index} {...accountManagementModal} />
      <AccountSettingModal {...accountSettingModal} />
    </ChakraProvider>
  )
}

export default App
