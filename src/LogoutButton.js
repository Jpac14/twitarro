import React, {useState, useRef} from "react"

import {
  Button,
  AlertDialog,
  AlertDialogOverlay,
  AlertDialogHeader,
  AlertDialogContent,
  AlertDialogBody,
  AlertDialogFooter,
  AlertDialogCloseButton,
  useToast,
} from "@chakra-ui/react"

import {auth} from "./firebase"
import {signOut} from "firebase/auth"

export function LogoutButton() {
  const [open, setOpen] = useState(false)
  const cancelRef = useRef()

  const toast = useToast()

  const logout = () => {
    signOut(auth)
      .then(() => {
        toast({
          title: "Logout Successful",
          status: "success",
          position: "bottom-left",
        })
        window.location.reload()
      })
      .catch((error) => {
        toast({
          title: `Error: ${error.code}`,
          description: error.message,
          status: "error",
          position: "bottom-left",
        })
      })
    setOpen(false)
  }

  return (
    <>
      <Button colorScheme="red" onClick={() => setOpen(true)}>
        Logout
      </Button>
      <AlertDialog
        isOpen={open}
        leastDestructiveRef={cancelRef}
        isCentered
        onClose={() => setOpen(false)}
      >
        <AlertDialogOverlay>
          <AlertDialogContent>
            <AlertDialogHeader fontSize="lg" fontWeight="bold">
              Logout
            </AlertDialogHeader>
            <AlertDialogCloseButton />
            <AlertDialogBody>Are you sure, you would like to logout?</AlertDialogBody>
            <AlertDialogFooter>
              <Button ref={cancelRef} onClick={() => setOpen(false)}>
                Cancel
              </Button>
              <Button
                colorScheme="red"
                onClick={logout}
                ml={3}
              >
                Logout
              </Button>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialogOverlay>
      </AlertDialog>
    </>
  )
}
