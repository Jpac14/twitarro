import {initializeApp} from "firebase/app"
import {getAnalytics} from "firebase/analytics"
import {getAuth} from "firebase/auth"
import {getFirestore} from "firebase/firestore"

const firebaseConfig = {
  apiKey: "AIzaSyBtQ1GYUZoiMOnuY12l8yrlsmlFLu10lX0",
  authDomain: "twitarro.firebaseapp.com",
  projectId: "twitarro",
  storageBucket: "twitarro.appspot.com",
  messagingSenderId: "1078464639652",
  appId: "1:1078464639652:web:226109bd3f19293e686c31",
  measurementId: "G-4PN6NH0V4B",
}

const app = initializeApp(firebaseConfig)
const analytics = getAnalytics(app)
const auth = getAuth(app)
const db = getFirestore(app)

export {app, analytics, auth, db}